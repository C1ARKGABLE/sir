"""
ODE solver visualization.
"""
# Imports
from scipy.integrate import odeint  # Primary function to solve ODEs
import numpy as np  # Numpy because Python range() function can't have float step size

# Boekh is our visualization tool, find out more at https://bokeh.org
from bokeh.io import curdoc
from bokeh.layouts import column, row
from bokeh.models import ColumnDataSource, Slider, TextInput
from bokeh.plotting import figure

curdoc().theme = "dark_minimal"  # Dark theme cause everyone loves dark mode

# Gotta setup these as global variables, they're mainly Slider objects and a float
global infection_rate, recovery_rate, immunity_rate, time

# Setup initial values for time range ODE will use
ODE_STEP_SIZE = 0.1
INITIAL_TIME_MAX = 100

time = np.arange(0, INITIAL_TIME_MAX, ODE_STEP_SIZE)

# These values correspond with the R_0 Wikipedia lists for COVID-19
# See https://en.wikipedia.org/wiki/Basic_reproduction_number
INITIAL_INFECTION_RATE = 1.0
INITIAL_RECOVERY_RATE = 0.25
INITIAL_IMMUNITY_RATE = 1.0


def sir_diff_eq(y, t0, infection_rate, recovery_rate, immunity_rate, n):
    """
    This is the system of equations that scipy.integrate.odeint() will solve.

    Check out the following for model specifics:

    https://en.m.wikipedia.org/wiki/Compartmental_models_in_epidemiology#The_SIR_model_with_vital_dynamics_and_constant_population
"""
    # Unpack the current values at timestep
    susceptible, infected, _ = y
    
    # Define susceptible, include recovered individuals that didn't gain immunity. 
    s = (
        recovery_rate * infected * (1 - immunity_rate)
        - (infection_rate * infected * susceptible) / n
    )
    # Define infected
    i = infection_rate * infected * susceptible / n - recovery_rate * infected
    # Define recovered, these are recovered individuals that have become immune.
    r = recovery_rate * infected * immunity_rate
    # Return the values
    return [s, i, r]


def do_ode(
    time,
    infection_rate=INITIAL_INFECTION_RATE,
    recovery_rate=INITIAL_RECOVERY_RATE,
    immunity_rate=INITIAL_IMMUNITY_RATE,
):
    """
    This is a helper function that did more in the past, but should be refactored. It currently just sets the initial population and calls the ODE function.
    See that documentation for more info about how this works.
    """
    sir0 = np.array([99, 1, 0])
    return odeint(
        sir_diff_eq,
        sir0,
        time,
        (infection_rate, recovery_rate, immunity_rate, sum(sir0)),
    )

# Get the results for the default values.
results = do_ode(time)

susceptible = ColumnDataSource(data=dict(x=time, y=results[:, 0]))
infected = ColumnDataSource(data=dict(x=time, y=results[:, 1]))
recovered = ColumnDataSource(data=dict(x=time, y=results[:, 2]))
si = ColumnDataSource(data=dict(x=results[:, 0], y=results[:, 1]))


def plot_time():
    plot = figure(
        plot_height=400,
        plot_width=800,
        title="SIR time_plot",
        tools="crosshair,pan,reset,save,wheel_zoom",
        x_range=[0, INITIAL_TIME_MAX],
        x_axis_label="Time",
        y_axis_label="Population",
    )
    plot.line(
        "x",
        "y",
        source=susceptible,
        legend_label="😷",
        line_color="yellow",
        line_width=3,
        line_alpha=0.6,
    )
    plot.line(
        "x",
        "y",
        source=infected,
        legend_label="🤒",
        line_color="green",
        line_width=3,
        line_alpha=0.6,
    )
    plot.line(
        "x",
        "y",
        source=recovered,
        legend_label="🏥",
        line_color="blue",
        line_width=3,
        line_alpha=0.6,
    )

    return plot


def plot_relation():
    plot = figure(
        plot_height=400,
        plot_width=800,
        title="SI Relation plot",
        tools="crosshair,pan,reset,save,wheel_zoom",
        # y_range=[-50, 50],
        # x_range=[0, INITIAL_TIME_MAX],
        x_axis_label="😷",
        y_axis_label="🤒",
    )
    plot.line(
        "x",
        "y",
        source=si,
        legend_label="SI",
        line_color="yellow",
        line_width=3,
        line_alpha=0.6,
    )
    return plot


infection_rate = Slider(
    title="Infection Rate", value=INITIAL_INFECTION_RATE, start=0, end=1, step=0.01
)
recovery_rate = Slider(
    title="Recovery Rate", value=INITIAL_RECOVERY_RATE, start=0, end=1, step=0.01
)
immunity_rate = Slider(
    title="Immunity Rate", value=INITIAL_IMMUNITY_RATE, start=0, end=1, step=0.01
)

plot = plot_time()  # plot_relation()  #


def update_data(attrname, old, new):
    global infection_rate, recovery_rate, immunity_rate, time

    results = do_ode(
        time,
        infection_rate=infection_rate.value,
        recovery_rate=recovery_rate.value,
        immunity_rate=immunity_rate.value,
    )

    susceptible.data = dict(x=time, y=results[:, 0])
    infected.data = dict(x=time, y=results[:, 1])
    recovered.data = dict(x=time, y=results[:, 2])
    si.data = dict(x=results[:, 0], y=results[:, 1])


for w in [
    infection_rate,
    recovery_rate,
    immunity_rate,
]:
    w.on_change("value", update_data)

inputs = column(infection_rate, recovery_rate, immunity_rate,)  # time_max)

curdoc().add_root(row(inputs, plot, width=800))
curdoc().title = "Gable sliders"
