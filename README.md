# SIR Model in Python

This is a simple interactive SIR model visualization. 

The initial state roughly models COVID-19, which partially inspired this project

## Here is a gif of it working:
![Gif of SIR Model workign](sir.gif)

## Instructions for running:

### Pre-Requisites:
1) Machine with Python3.* (Default installed on some operating systems, check with [Python](https://wiki.python.org/moin/BeginnersGuide/Download))
2) Git installed
3) Internet connection
### Installation
In a terminal or commandline use the following commands:
1) `git clone https://gitlab.com/C1ARKGABLE/sir` to download my code.
2) `cd sir` to change directories into the new directory.
3) `pip3 install -r requirements.txt` to install the requirements for this code. ([Bokeh](https://bokeh.org), [numpy](https://numpy.org), [scipy](https://www.scipy.org))
4) `python3 -m bokeh serve sir.py --dev sir.py` to use Bokeh's `serve` command to start the code. (The `--dev` tag reloads the page when there are changes made to `sir.py`)

After this, you should see five debug lines. One (the second to last) line contains the url (`http://localhost`) where this visualization is hosted. Follow it to view the visualization.
